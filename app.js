const express = require("express");
const organizationRouter = require("./routers/organization.router.js");
const formRouter = require("./routers/form.router.js");

const app = express();

app.use(express.json());
app.use("/" , organizationRouter);
app.use("/", formRouter);


app.listen(3000);
