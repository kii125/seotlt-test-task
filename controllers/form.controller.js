const {Form, Organization} = require("../models/index.js");
const { Op } = require("sequelize");
const requestOptions = require("./requestOptions.js");

class formController{
    async createForm(req,res){
        const orgId = req.params.organizationId;
        try{
            const org = await Organization.findOne({where : {id : orgId}});
            req.body.organizationId = orgId;
            const createForm = await Form.create(req.body);
            return res.json({status : "created" , id : createForm.id});
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async getForms(req,res){
        try{
            let options = requestOptions(req);
            if(req.query.quarter){
                options.where = {
                    quarter:{
                        [Op.and]: {
                            [Op.gt]: req.query.quarter,
                            [Op.lt]: req.query.quarter + "-12-31"
                        }
                    }
                }
            }
            const allForms = await Form.findAll(options);
            return res.json(allForms);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async getFormById(req,res){
        const id = req.params.id;
        try{
            let options = requestOptions(req);
            options.where = {id}
            const fromById = await Form.findOne(options);
            return res.json(fromById);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async getFormByOrganizationId(req,res){
        const organizationId = req.params.organizationId;
        try{
            let options = requestOptions(req);
            options.where = {organizationId};
            if(req.query.quarter){
                options.where = {
                    organizationId,
                    quarter:{
                        [Op.and]: {
                            [Op.gt]: req.query.quarter,
                            [Op.lt]: req.query.quarter + "-12-31"
                        }
                    }
                }
            }
            const fromById = await Form.findAll(options);
            return res.json(fromById);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async updateForm(req,res){
        const id = req.params.id;
        try{
            const updateFormById = await Form.findOne({where : {id}});
            Object.assign(updateFormById , req.body)
            updateFormById.save();
            return res.json(updateFormById);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async deleteForm(req,res){
        const id = req.params.id;
        try{
            const deleteFormById = await Form.findOne({where : {id}});
            await deleteFormById.destroy();
            return res.json({status: "deleted"});
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
}


module.exports = new formController();