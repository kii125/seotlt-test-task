const {Form, Organization} = require("../models/index.js");
const requestOptions = require("./requestOptions.js");

class organizationController {
    async createOrganization(req, res){
        try{
            const newOrganization = await Organization.create(req.body);
            return res.json({status: "Created", id: newOrganization.id});
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async getOrganizations(req, res){
        try{
            let options = requestOptions(req);
            const allOrganizations = await Organization.findAll(options);
            return res.json(allOrganizations);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async getOrganizationById(req, res){
        const id = req.params.id;
        try{
            let options = requestOptions(req);
            options.where = {id};
            const organizationById = await Organization.findOne(options);
            return res.json(organizationById);
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async updateOrganization(req, res){
        const id = req.params.id;
        try{
            const updateOrganizationById = await Organization.findOne({where : {id}});
            Object.assign(updateOrganizationById , req.body)
            updateOrganizationById.save();
            return res.json({status: "Updated"});
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
    async deleteOrganization(req, res){
        const id = req.params.id;
        try{
            const deleteOrganizationById = await Organization.findOne({where : {id}});
            await deleteOrganizationById.destroy();
            return res.json({status: "deleted"});
        }catch(err){
            console.log(err);
            return res.status(500).json(err);
        }
    }
}

module.exports = new organizationController();