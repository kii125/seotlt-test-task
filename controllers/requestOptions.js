function requestOptions(req){
    let options = {};

    if(req.query.filter){
        options.attributes = req.query.filter.split(",");
    }
    if(req.query.sort){
        options.order = [
            [req.query.sort , "ASC"]
        ];
    }

    return options;
}

module.exports = requestOptions;
