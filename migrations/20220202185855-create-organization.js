'use strict';
module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('organizations', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      organizationName: {
        type: DataTypes.STRING
      },
      ownshipForm: {
        type: DataTypes.STRING
      },
      address: {
        type: DataTypes.STRING
      },
      mandatoryAudit: {
        type: DataTypes.BOOLEAN
      },
      individualAuditor: {
        type: DataTypes.STRING
      },
      TIN: {
        type: DataTypes.STRING
      },
      PSRN: {
        type: DataTypes.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('organizations');
  }
};