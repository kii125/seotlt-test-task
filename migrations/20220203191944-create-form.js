'use strict';
module.exports = {
  async up(queryInterface, DataTypes) {
    await queryInterface.createTable('forms', {
      id: {
        allowNull: false,
        autoIncrement: true,
        primaryKey: true,
        type: DataTypes.INTEGER
      },
      organizationId : {
        type : DataTypes.INTEGER,
        allowNull:false,
      },
      quarter: {
        type: DataTypes.DATE
      },
      intangibleAsset: {
        type: DataTypes.STRING
      },
      researchAndDevelopmentResults: {
        type: DataTypes.STRING
      },
      intangibleSearchAssets: {
        type: DataTypes.STRING
      },
      tangibleSearchAssets: {
        type: DataTypes.STRING
      },
      permanentAssets: {
        type: DataTypes.STRING
      },
      profitableInvestmentsInMaterialValues: {
        type: DataTypes.STRING
      },
      financialInvestments: {
        type: DataTypes.STRING
      },
      DeferredTaxAssets: {
        type: DataTypes.STRING
      },
      otherNonCurrentAssets: {
        type: DataTypes.STRING
      },
      section1Total: {
        type: DataTypes.STRING
      },
      reserves: {
        type: DataTypes.STRING
      },
      valueAddedTaxOnAcquiredValuables: {
        type: DataTypes.STRING
      },
      accountsReceivable: {
        type: DataTypes.STRING
      },
      financialInvestments: {
        type: DataTypes.STRING
      },
      cashAndCashEquivalents: {
        type: DataTypes.STRING
      },
      otherCurrentAssets: {
        type: DataTypes.STRING
      },
      selection2Total: {
        type: DataTypes.STRING
      },
      balance: {
        type: DataTypes.STRING
      },
      authorizedCapital: {
        type: DataTypes.STRING
      },
      ownSharesRepurchasedFromShareholders: {
        type: DataTypes.STRING
      },
      revaluationOfNonCurrentAssets: {
        type: DataTypes.STRING
      },
      aditionalCapital: {
        type: DataTypes.STRING
      },
      reserveCapital: {
        type: DataTypes.STRING
      },
      retainedEarnings: {
        type: DataTypes.STRING
      },
      selection3Total: {
        type: DataTypes.STRING
      },
      borrowedFundsLong: {
        type: DataTypes.STRING
      },
      deferredTaxLiabilities: {
        type: DataTypes.STRING
      },
      estimatedLiabilities: {
        type: DataTypes.STRING
      },
      otherliabilities: {
        type: DataTypes.STRING
      },
      selection4Total: {
        type: DataTypes.STRING
      },
      borrowedFundsShort: {
        type: DataTypes.STRING
      },
      accountsPayable: {
        type: DataTypes.STRING
      },
      revenueOfTheFuturePeriods: {
        type: DataTypes.STRING
      },
      estimatedLiabilities: {
        type: DataTypes.STRING
      },
      otherLiabilities: {
        type: DataTypes.STRING
      },
      selection5Total: {
        type: DataTypes.STRING
      },
      balance: {
        type: DataTypes.STRING
      },
      createdAt: {
        allowNull: false,
        type: DataTypes.DATE
      },
      updatedAt: {
        allowNull: false,
        type: DataTypes.DATE
      }
    });
  },
  async down(queryInterface, Sequelize) {
    await queryInterface.dropTable('forms');
  }
};