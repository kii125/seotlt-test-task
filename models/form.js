'use strict';
const { INTEGER } = require('sequelize');
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Form extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Form.belongsTo(models.Organization , {foreignKey : "organizationId"});
    }
  }
  Form.init({
    organizationId:DataTypes.INTEGER,
    quarter: DataTypes.DATE,
    intangibleAsset: DataTypes.STRING,
    researchAndDevelopmentResults: DataTypes.STRING,
    intangibleSearchAssets: DataTypes.STRING,
    tangibleSearchAssets: DataTypes.STRING,
    permanentAssets: DataTypes.STRING,
    profitableInvestmentsInMaterialValues: DataTypes.STRING,
    DeferredTaxAssets: DataTypes.STRING,
    otherNonCurrentAssets: DataTypes.STRING,
    section1Total: DataTypes.STRING,
    reserves: DataTypes.STRING,
    valueAddedTaxOnAcquiredValuables: DataTypes.STRING,
    accountsReceivable: DataTypes.STRING,
    financialInvestments: DataTypes.STRING,
    cashAndCashEquivalents: DataTypes.STRING,
    otherCurrentAssets: DataTypes.STRING,
    selection2Total: DataTypes.STRING,
    authorizedCapital: DataTypes.STRING,
    ownSharesRepurchasedFromShareholders: DataTypes.STRING,
    revaluationOfNonCurrentAssets: DataTypes.STRING,
    aditionalCapital: DataTypes.STRING,
    reserveCapital: DataTypes.STRING,
    retainedEarnings: DataTypes.STRING,
    selection3Total: DataTypes.STRING,
    borrowedFundsLong: DataTypes.STRING,
    deferredTaxLiabilities: DataTypes.STRING,
    otherliabilities: DataTypes.STRING,
    selection4Total: DataTypes.STRING,
    borrowedFundsShort: DataTypes.STRING,
    accountsPayable: DataTypes.STRING,
    revenueOfTheFuturePeriods: DataTypes.STRING,
    estimatedLiabilities: DataTypes.STRING,
    otherLiabilities: DataTypes.STRING,
    selection5Total: DataTypes.STRING,
    balance: DataTypes.STRING
  }, {
    sequelize,
    tableName: "forms",
    modelName: 'Form',
  });
  return Form;
};