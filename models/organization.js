'use strict';
const {
  Model
} = require('sequelize');
module.exports = (sequelize, DataTypes) => {
  class Organization extends Model {
    /**
     * Helper method for defining associations.
     * This method is not a part of Sequelize lifecycle.
     * The `models/index` file will call this method automatically.
     */
    static associate(models) {
      // define association here
      models.Organization.hasMany(models.Form , {foreignKey : "organizationId"});
    }
  }
  Organization.init({
    organizationName: DataTypes.STRING,
    ownshipForm: DataTypes.STRING,
    address: DataTypes.STRING,
    mandatoryAudit: DataTypes.BOOLEAN,
    individualAuditor: DataTypes.STRING,
    TIN: DataTypes.STRING,
    PSRN: DataTypes.STRING
  }, {
    sequelize,
    tableName: "organizations",
    modelName: 'Organization',
  });
  return Organization;
};