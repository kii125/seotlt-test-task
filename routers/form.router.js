const Router = require("express");
const formController = require("../controllers/form.controller.js");
const router = new Router();

router.post("/:organizationId/createForm", formController.createForm);
router.get("/forms", formController.getForms);
router.get("/organization/:organizationId/forms", formController.getFormByOrganizationId);
router.get("/form/:id", formController.getFormById);
router.put("/form/:id", formController.updateForm);
router.delete("/form/:id", formController.deleteForm);

module.exports = router;