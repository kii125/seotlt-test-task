const Router = require("express");
const organizationController = require("../controllers/organization.controller.js");
const router = new Router();

router.post("/organization", organizationController.createOrganization);
router.get("/organizations", organizationController.getOrganizations);
router.get("/organization/:id", organizationController.getOrganizationById);
router.put("/organization/:id", organizationController.updateOrganization);
router.delete("/organization/:id", organizationController.deleteOrganization);

module.exports = router;