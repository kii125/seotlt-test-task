const {Request, Responce} = require("express");
const generate = require("./generator.js");
const organizationController = require("../controllers/organization.controller.js");
const formController = require("../controllers/form.controller.js");


let organizationTestId;
let organizationParams = {
    organizationName : generate(10), 
    ownshipForm: generate(10), 
    address: generate(10), 
    mandatoryAudit: false, 
    individualAuditor: generate(10), 
    TIN: generate(10), 
    PSRN: generate(10)
};

let formTestId;
let formParams = {
    quarter: 2021-4-5,
    intangibleAsset: generate(20),
    researchAndDevelopmentResults: generate(10),
    intangibleSearchAssets: generate(10),
    tangibleSearchAssets: generate(10),
    permanentAssets: generate(10),
    profitableInvestmentsInMaterialValues: generate(10),
    DeferredTaxAssets: generate(10),
    otherNonCurrentAssets: generate(10),
    section1Total: generate(10),
    reserves: generate(10),
    valueAddedTaxOnAcquiredValuables: generate(10),
    accountsReceivable: generate(10),
    financialInvestments: generate(10),
    cashAndCashEquivalents: generate(10),
    otherCurrentAssets: generate(10),
    selection2Total: generate(10),
    authorizedCapital: generate(10),
    ownSharesRepurchasedFromShareholders: generate(10),
    revaluationOfNonCurrentAssets: generate(10),
    aditionalCapital: generate(10),
    reserveCapital: generate(10),
    retainedEarnings: generate(10),
    selection3Total: generate(10),
    borrowedFundsLong: generate(10),
    deferredTaxLiabilities: generate(10),
    otherliabilities: generate(10),
    selection4Total: generate(10),
    borrowedFundsShort: generate(10),
    accountsPayable: generate(10),
    revenueOfTheFuturePeriods: generate(10),
    estimatedLiabilities: generate(10),
    otherLiabilities: generate(10),
    selection5Total: generate(10),
    balance: generate(10)
};

let responseObject = {};
const response = {
    json: jest.fn().mockImplementation((result) => {
    responseObject = result;
})
}

describe("organization tests", () =>{
    test("Create organization", async () => {
        await organizationController.createOrganization({body : organizationParams},response);
        organizationTestId = responseObject.id;
        expect(responseObject.status).toEqual("Created");
    });
    
    test("Get organization by id", async () => {
        await organizationController.getOrganizationById({params : {id : organizationTestId}, query : {filter : null , sort : null}},response);
        expect(responseObject.TIN).toEqual(organizationParams.TIN);
    });
    
    test("Update organization" , async () => {
        await organizationController.updateOrganization({params : { id : organizationTestId}, body : { organizationName : generate(10) }},response);
        expect(responseObject.status).toEqual("Updated");
    });
    
    test(
        "Delete organization", async () =>{
            await organizationController.deleteOrganization({params : { id : organizationTestId}},response);
            expect(responseObject.status).toEqual("deleted");
    });
});


describe("form tests", () =>{ 
    test("Create form", async () =>{
        await formController.createForm({params : {organizationId : organizationTestId}, body : formParams},response);
        expect(responseObject.status).toEqual("created");
        formTestId = responseObject.id;
    });

    test("Get form by id", async () => {
        await formController.getFormById({params : {id : formTestId} , query : {filter : null , sort : null}},response);

        expect(responseObject.intangibleAsset).toEqual(formParams.intangibleAsset);
    });

    test("Update form", async () => {
        await formController.updateForm({params : {id : formTestId}, body : { otherNonCurrentAssets: "test update form",}},response);
        expect(responseObject.otherNonCurrentAssets).toEqual("test update form");
    })

    test("Delete form", async () => {

        await formController.deleteForm({params : {id : formTestId}},response);
        expect(responseObject.status).toEqual("deleted");
    });
});